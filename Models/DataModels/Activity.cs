using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace room_reservation.Models.DataModels
{
    public class Activity{
        public long id { get; set; }
        [StringLength(50)]
        public string Title { get; set; }
        [StringLength(300)]
        public string Description { get; set; }
    }
}